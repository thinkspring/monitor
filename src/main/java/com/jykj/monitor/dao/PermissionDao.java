package com.jykj.monitor.dao;

import com.jykj.monitor.domain.Permission;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * TODO: 用户权限数据操作层
 * @author wgbing
 * @date 2018/7/12 下午9:59
 */
public interface PermissionDao extends JpaRepository<Permission, Long> {
    List<String> findPermissionKeysByUserId(Long id);
}
