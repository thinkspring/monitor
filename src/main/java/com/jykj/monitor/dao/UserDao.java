package com.jykj.monitor.dao;


import com.jykj.monitor.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * TODO: 用户数据操作层
 * @author wgbing
 * @date 2018/7/12 下午9:59
 */
public interface UserDao extends JpaRepository<User, Long> {
    @Query(" select u from User u where u.deleted = false and u.loginName =?1 and u.type=?2 ")
    User findUserByUsernameAndType(String username, Integer type);
}
