package com.jykj.monitor.utils;

import cn.hutool.core.io.file.FileReader;
import cn.hutool.core.util.StrUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class STFReaderHandlerImpl implements FileReader.ReaderHandler {
    @Override
    public Map<String,String> handle(BufferedReader reader) throws IOException {
        Map<String,String> map = new LinkedHashMap<>();
        String line = null;
        while ((line = reader.readLine()) != null){
            if(StrUtil.isNotEmpty(line)){
                if(StrUtil.containsAny(line,"=")){
                    List<String> lineList = StrUtil.split(line,'=',true,false);
                    if(lineList != null && !lineList.isEmpty()){
                        if(lineList.size()==2){
                            map.put(lineList.get(0),lineList.get(1));
                        }
                    }
                }else {
                    map.put("SectionName",line);
                }
            }
        }
        return map;
    }
}
