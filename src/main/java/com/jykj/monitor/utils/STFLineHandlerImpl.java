package com.jykj.monitor.utils;

import cn.hutool.core.io.LineHandler;
import cn.hutool.core.util.StrUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class STFLineHandlerImpl implements LineHandler {
    public Map<String,String> lineMap = new HashMap<>();
    @Override
    public void handle(String line) {
        if(StrUtil.isNotEmpty(line)){
            if(StrUtil.containsAny(line,"=")){
                List<String> lineList = StrUtil.split(line,'=',true,false);
                if(lineList.size()==2){
                    lineMap.put(lineList.get(0),lineList.get(1));
                }
            }
        }
        return;
    }

    public static void main(String[] args) {
        String str = "key1= value1";
        STFLineHandlerImpl aa = new STFLineHandlerImpl();
        aa.handle(str);
        System.out.println(aa.lineMap);
    }
}
