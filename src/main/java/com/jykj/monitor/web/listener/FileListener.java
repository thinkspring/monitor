package com.jykj.monitor.web.listener;

import com.jykj.monitor.service.ListenerService;
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class FileListener extends FileAlterationListenerAdaptor {
    private static final Logger log = LoggerFactory.getLogger(FileListener.class);
    // 声明业务服务
    private ListenerService listenerService;

    // 采用构造函数注入服务
    public FileListener(ListenerService listenerService) {
        this.listenerService = listenerService;
    }

    //轮询开始
    @Override
    public void onStart(FileAlterationObserver observer) {
        log.info("轮询开始!");
    }

    //目录创建执行
    @Override
    public void onDirectoryCreate(File directory) {
        super.onDirectoryCreate(directory);
    }

    //目录修改执行
    @Override
    public void onDirectoryChange(File directory) {
        super.onDirectoryChange(directory);
    }

    //目录删除执行
    @Override
    public void onDirectoryDelete(File directory) {
        super.onDirectoryDelete(directory);
    }

    //文件创建执行
    @Override
    public void onFileCreate(File file) {
        log.info("=====文件创建=====");
    }

    //文件修改执行
    @Override
    public void onFileChange(File file) {
        log.info("文件修改!");
    }

    //文件删除执行
    @Override
    public void onFileDelete(File file) {
        super.onFileDelete(file);
    }

    //轮询结束
    @Override
    public void onStop(FileAlterationObserver observer) {
        super.onStop(observer);
    }
}
