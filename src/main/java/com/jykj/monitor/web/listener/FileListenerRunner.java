package com.jykj.monitor.web.listener;

import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class FileListenerRunner implements CommandLineRunner {
    private static final Logger log = LoggerFactory.getLogger(FileListenerRunner.class);
    @Autowired
    private FileListenerFactory fileListenerFactory;

    @Override
    public void run(String... strings) throws Exception {
        // 创建监听者
        FileAlterationMonitor fileAlterationMonitor = fileListenerFactory.getMonitor();
        try {
            // do not stop this thread
            log.info("======================监听者开启==========================");
            fileAlterationMonitor.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
