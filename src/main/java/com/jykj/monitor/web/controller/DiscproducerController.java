package com.jykj.monitor.web.controller;

import com.jykj.monitor.service.DiscproducerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/discproducer")
public class DiscproducerController {
    private static final Logger log = LoggerFactory.getLogger(DiscproducerController.class);

    @Autowired
    private DiscproducerService discproducerService;

    @GetMapping("/start")
    public String start(){
        log.info("===========刻录开始=============");
        discproducerService.createJDF();
        return "start!!!";
    }

    @GetMapping("/end")
    public String end(){
        log.info("===========刻录结束=============");
        return "end!!!";
    }

    @GetMapping("/getSTFStatus")
    public String getSTFStatus(){
        discproducerService.readSTF();
        return "status...";
    }

}
