package com.jykj.monitor.service;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileReader;
import cn.hutool.core.io.file.FileWriter;
import com.jykj.monitor.utils.STFLineHandlerImpl;
import com.jykj.monitor.utils.STFReaderHandlerImpl;
import com.sun.org.apache.xml.internal.serialize.LineSeparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional
public class DiscproducerService {
    private static final Logger log = LoggerFactory.getLogger(DiscproducerService.class);
    //监听路径
    @Value("${dir.monitorDir}")
    private String monitorDir;

    public void createJDF(){
        File file = FileUtil.file(monitorDir, "aa.jdf");
        FileWriter fileWriter = FileWriter.create(file);
        Map<String,Object> map = new HashMap<>();
        map.put("key1","value1");
        map.put("key2","value2");
        map.put("key3","value3");
        map.put("key4","value4");
        fileWriter.writeMap(map,"=",true);
        log.info("创建JDF文件");
    }

    public void readSTF() {
        FileReader fileReader = new FileReader(monitorDir+ File.separator+"aa.stf");
//        STFLineHandlerImpl lineHandler = new STFLineHandlerImpl();
//        fileReader.readLines(lineHandler);
//        System.out.println(lineHandler.lineMap);
        STFReaderHandlerImpl readerHandler = new STFReaderHandlerImpl();
        System.out.println(fileReader.read(readerHandler));
    }
}
