package com.jykj.monitor.vo;

import com.jykj.monitor.domain.User;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户认证角色
 */
public class AuthUserVo implements Serializable {
    private Long id;
    private String email;
    private String mobile;
    private String realName;
    private Date createTime;

    public AuthUserVo() {}

    public AuthUserVo(User user) {
        if(null != user) {
            this.id = user.getId();
            this.email = user.getEmail();
            this.mobile = user.getMobile();
            this.realName = user.getRealName();
            this.createTime = user.getCreateTime();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
