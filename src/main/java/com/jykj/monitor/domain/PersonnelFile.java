package com.jykj.monitor.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

/**
 * TODO: 场所出入人员档案管理，包括办公人员或嫌疑人基本信息实体
 * @author songzihui
 * @date 2018/9/19 8:39
 */
@Entity
@Table(name="so_personnel_file")
public class PersonnelFile {
    /*ID*/
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    /* 真实姓名 */
    @Column(name = "real_name")
    private String realName;
    /* 身份证号 */
    @Column(name = "id_card_no")
    private String idCardNo;
    /* 出生日期 */
    @Column (name = "birthday")
    private Date birthday;
    /* 性别：1：代表男；0：代表女 */
    @Column(name = "sex")
    private Integer sex;
    /*证件照*/
    @Column(name = "picture")
    private Long picture;
    /* 民族 关联Dictionary 父节点config_key=MZ（民族） */
    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Dictionary.class)
    @JoinColumn(name = "nation_id")
    private Dictionary nation;
    /*籍贯地，出生地*/
    @Column(name = "birth_place")
    private String birthPlace;
    /*是否删除*/
    @Column(name = "deleted")
    private Boolean deleted;
    /*创建者 */
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class)
    @JoinColumn(name = "creator_id")
    private User creator;
    /* 创建时间 */
    @Column (name = "create_time")
    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(String idCardNo) {
        this.idCardNo = idCardNo;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Long getPicture() {
        return picture;
    }

    public void setPicture(Long picture) {
        this.picture = picture;
    }

    public Dictionary getNation() {
        return nation;
    }

    public void setNation(Dictionary nation) {
        this.nation = nation;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
