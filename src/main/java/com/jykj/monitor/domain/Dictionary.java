package com.jykj.monitor.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

/**
 * 数据字典
 */
@Entity
@Table(name = "so_dictionary")
public class Dictionary {
    /* ID */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    /* 配置Key值 */
    @Column(name = "config_key")
    private String configKey;
    /* 字典项名称 */
    @Column(name = "name", nullable = false)
    private String name;
    /* 国标编码 */
    @Column(name = "code")
    private String code;
    /* 字典项值 */
    @Column(name = "value")
    private String value;
    /* 备注 */
    @Column(name = "note")
    private String note;
    /* 是否锁定 */
    @Column(name = "locked", nullable = false)
    private Boolean locked;
    /* 父级字典项 *//*
    @Column(name = "parent_id")
    private Long parentId;*/
    /* 排序字段 */
    @Column(name = "order_no")
    private Integer orderNo;
    /* 父级字典项 */
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    private Dictionary parent;
    /* 子级字典项列表 */
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parent", cascade = {CascadeType.MERGE}, targetEntity = Dictionary.class)
    @OrderBy("id ASC")
    private Set<Dictionary> children;

    public Dictionary getParent() {
        return parent;
    }

    public void setParent(Dictionary parent) {
        this.parent = parent;
    }

    public Set<Dictionary> getChildren() {
        return children;
    }

    public void setChildren(Set<Dictionary> children) {
        this.children = children;
    }

    public Integer getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Integer orderNo) {
        this.orderNo = orderNo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }


    @Override
    public String toString() {
        return "Dictionary{" +
                "id=" + id +
                ", configKey='" + configKey + '\'' +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", value='" + value + '\'' +
                ", note='" + note + '\'' +
                ", locked=" + locked +
                ", orderNo=" + orderNo +
                '}';
    }
}
